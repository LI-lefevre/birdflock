﻿using Model;
using SkiaSharp;
using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Viewer
{
    public static class Renderer
    {
        public static void Render(SKCanvas canvas, Game game, bool tails, bool ranges, bool direction)
        {
            DrawBackground(game, canvas);

            foreach (Boid boid in game.Boids)
            {
                SKColor color = new SKColor(boid.color.R, boid.color.G, boid.color.B, boid.color.A);
                if (boid.isDead)
                    color = SKColors.Red;

                if (tails)
                    DrawTailLines(boid, canvas, color);
                if (boid.isDead)
                    DrawDeadBoid(game, boid, canvas, color);
                else
                {
                    if (direction)
                        DrawBoidDirection(game, boid, canvas, color);
                    else
                        DrawBoidSimple(boid, canvas, color);
                }
            }

            if (ranges)
                DrawRanges(game, canvas);
        }

        private static void DrawBackground(Game game, SKCanvas canvas)
        {
            canvas.Clear(SKColor.Parse("#0080ff"));


            using (var paint = new SKPaint() { IsAntialias = true, Color = SKColors.Black })
            {
                canvas.SaveLayer();
                foreach (Rectangle rectangle in game.BackgroundArea.AllLimits)
                {
                    canvas.DrawRect(rectangle.X, rectangle.Y, rectangle.Width, rectangle.Height, paint);
                }

                canvas.Restore();
            }
        }

        private static void DrawTailDots(Boid boid, SKCanvas canvas, SKColor color)
        {
            using (var paint = new SKPaint() { IsAntialias = true })
            {
                for (int i = 0; i < boid.positions.Count; i++)
                {
                    double frac = (i + 1d) / (boid.positions.Count);
                    byte alpha = (byte)(255 * frac * .5);
                    paint.Color = new SKColor(color.Red, color.Green, color.Blue, alpha);
                    var pos = boid.positions[i];
                    canvas.DrawCircle((float)pos.X, (float)pos.Y, 2, paint);
                }
            }
        }

        private static void DrawTailLines(Boid boid, SKCanvas canvas, SKColor color)
        {
            using (var paint = new SKPaint() { IsAntialias = true, StrokeWidth = 2 })
            {
                for (int i = 1; i < boid.positions.Count; i++)
                {
                    double frac = (i + 1d) / (boid.positions.Count);
                    byte alpha = (byte)(255 * frac * .5);
                    paint.Color = new SKColor(color.Red, color.Green, color.Blue, alpha);
                    canvas.DrawLine(
                        x0: (float)boid.positions[i - 1].X,
                        y0: (float)boid.positions[i - 1].Y,
                        x1: (float)boid.positions[i].X,
                        y1: (float)boid.positions[i].Y,
                        paint: paint);
                }
            }
        }

        private static void DrawBoidDirection(Game game, Boid boid, SKCanvas canvas, SKColor color)
        {
            SKPath boidPathDirection = new SKPath();
            boidPathDirection.MoveTo(0, 0);
            boidPathDirection.LineTo(-5, -2);
            boidPathDirection.LineTo(0, 8);
            boidPathDirection.LineTo(5, -2);
            boidPathDirection.LineTo(0, 0);

            using (var paint = new SKPaint() { IsAntialias = true, Color = color })
            {
                canvas.SaveLayer();
                //canvas.SaveLayer(new SKRect(0, 0, (float)field.Width, (float)field.Height), paint);
                canvas.Translate((float)boid.Pos.X, (float)boid.Pos.Y);
                canvas.RotateDegrees((float)boid.Vel.GetAngle());
                if (boid is Predator)
                    canvas.Scale(1.5f);
                canvas.DrawPath(boidPathDirection, paint);
                canvas.Restore();
            }
            boidPathDirection.Dispose();
        }

        private static void DrawDeadBoid(Game field, Boid boid, SKCanvas canvas, SKColor color)
        {
            SKPath boidPathDirection = new SKPath();
            boidPathDirection.MoveTo(5, 2);
            boidPathDirection.LineTo(5, -2);
            boidPathDirection.LineTo(2, -2);
            boidPathDirection.LineTo(2, -5);
            boidPathDirection.LineTo(-2, -5);
            boidPathDirection.LineTo(-2, -2);
            boidPathDirection.LineTo(-5, -2);
            boidPathDirection.LineTo(-5, 2);

            boidPathDirection.LineTo(-2, 2);
            boidPathDirection.LineTo(-2, 5);
            boidPathDirection.LineTo(2, 5);
            boidPathDirection.LineTo(2, 2);
            boidPathDirection.LineTo(5, 2);

            using (var paint = new SKPaint() { IsAntialias = true, Color = color })
            {
                canvas.SaveLayer();
                //canvas.SaveLayer(new SKRect(0, 0, (float)field.Width, (float)field.Height), paint);
                canvas.Translate((float)boid.Pos.X, (float)boid.Pos.Y);
                canvas.RotateDegrees(45);
                canvas.Scale(1.3f);
                canvas.DrawPath(boidPathDirection, paint);
                canvas.Restore();
            }
            boidPathDirection.Dispose();
        }

        private static void DrawBoidSimple(Boid boid, SKCanvas canvas, SKColor color, float radius = 4)
        {
            using (var paint = new SKPaint() { IsAntialias = true, Color = color })
            {
                canvas.DrawCircle((float)boid.Pos.X, (float)boid.Pos.Y, radius, paint);
            }
        }

        private static void DrawRanges(Game field, SKCanvas canvas)
        {
            Boid boid = field.Boids.Where(x => x is Predator).Where(x => (x as Predator).mousePredator).ToList().FirstOrDefault();
            if (boid == null) boid = field.Boids.FirstOrDefault();
            if (boid == null) return;
            float vision = (float)boid.Vision;
            float repel = (float)boid.hitbox;

            using (var paint = new SKPaint() { IsAntialias = true, StrokeWidth = 2, IsStroke = true })
            {
                canvas.SaveLayer();
                // canvas.SaveLayer(new SKRect(0, 0, (float)field.Width, (float)field.Height), paint);
                canvas.Translate((float)boid.Pos.X, (float)boid.Pos.Y);
                canvas.RotateDegrees((float)boid.Vel.GetAngle());

                paint.Color = SKColors.Yellow;
                canvas.DrawLine(-5, 0, 5, 0, paint);
                canvas.DrawLine(0, 0, 0, 10, paint);

                paint.Color = SKColors.Magenta;
                canvas.DrawCircle(0, 0, repel, paint);
                canvas.DrawLine(0, repel - 5, 0, repel + 5, paint);

                paint.Color = SKColors.LightGreen;
                canvas.DrawCircle(0, 0, vision, paint);
                canvas.DrawLine(0, vision - 10, 0, vision + 10, paint);

                canvas.Restore();
            }
        }
    }
}
