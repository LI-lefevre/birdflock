﻿using Model;
using Model.Environnement;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace Viewer
{
    public partial class Form1 : Form
    {
        private static readonly int fpsToRemember = 50;

        public Form1()
        {
            InitializeComponent();
            Reset();
        }
        Queue<double> fpsList = new Queue<double>(fpsToRemember);

        public Game currentGame;
        private void Reset()
        {
            GameFinishedTextBox.Visible = false;
            List<Rectangle> Walls = new List<Rectangle>();
            Walls.Add(new Rectangle(0, 100, skglControl1.Width-100, 5));
            Walls.Add(new Rectangle(100, 200, skglControl1.Width, 5));
            Walls.Add(new Rectangle(0, 300, skglControl1.Width-100, 5));
            Walls.Add(new Rectangle(100, 400, skglControl1.Width, 5));
            currentGame = new Game(new BackgroundArea(skglControl1.Width, skglControl1.Height, Walls),
                width: skglControl1.Width,
                height: skglControl1.Height,
                boidCount: (int)BoidCountNud.Value,
                predatorCount: (int)PredatorCountNud.Value,
                random: RandomCheckbox.Checked);
            currentGame.start();
            AttractTrackbar_Scroll(null, null);
            AvoidTrackbar_Scroll(null, null);
            AlignTrackbar_Scroll(null, null);
            VisionTrackbar_Scroll(null, null);
        }
        private void skglControl1_SizeChanged(object sender, EventArgs e)
        {
            currentGame.Width = skglControl1.Width;
            currentGame.Height = skglControl1.Height;
        }
        private void ResetButton_Click(object sender, EventArgs e) => Reset();
        private void BoidCountNud_ValueChanged(object sender, EventArgs e)
        {
            currentGame.Boids.RemoveRange(0, currentGame.Boids.Count - (int)BoidCountNud.Value > 0 ? currentGame.Boids.Count - (int)BoidCountNud.Value : 0);
            if (-currentGame.Boids.Count + (int)BoidCountNud.Value > 0)
            {
                Random rand = RandomCheckbox.Checked ? new Random() : new Random(0);
                List<Boid> boidList = new List<Boid>();
                for (int i = 0; i < (-currentGame.Boids.Count + (int)BoidCountNud.Value); i++)
                    boidList.Add(new Prey(Width, Height, rand));
                currentGame.Boids.AddRange(boidList);
            }
            PredatorCountNud.Maximum = BoidCountNud.Value;
        }

        private void PredatorCountNud_ValueChanged(object sender, EventArgs e)
        {
            for (int i = 0; i < currentGame.Boids.Count(); i++)
                if (i < PredatorCountNud.Value)
                    currentGame.Boids[i] = new Predator(currentGame.Boids[i]);
                else
                    currentGame.Boids[i] = new Prey(currentGame.Boids[i]);
        }

        private readonly Stopwatch stopwatch = new Stopwatch();
        private void timer1_Tick(object sender, EventArgs e)
        {
            stopwatch.Restart();
            Point point = skglControl1.PointToClient(Cursor.Position);

            currentGame.Advance(point, SpeedTrackbar.Value * .2);

            if (currentGame.GameStatus.Equals(Game.GameStatusEnum.victory))
            {
                Cursor.Show();
                GameFinishedTextBox.Visible = true;
                GameFinishedTextBox.Text =
                    "You won!!" + Environment.NewLine + Environment.NewLine +
                    "Game timeout: " + currentGame.timeOut + "seconds" + Environment.NewLine +
                    "Game timer: " + currentGame.GameTimer.ElapsedMilliseconds / 1000 + "seconds" + Environment.NewLine +
                    "Killed ennemy: " + currentGame.Boids.Where(x => x.isDead).Count() + Environment.NewLine +
                    "Allies: " + currentGame.Boids.Where(x => x is Predator).Count() + Environment.NewLine;
            }
            if (currentGame.GameStatus.Equals(Game.GameStatusEnum.lost))
            {
                Cursor.Show();
                GameFinishedTextBox.Visible = true;
                GameFinishedTextBox.Text =
                    "You loose!!" + Environment.NewLine + Environment.NewLine +
                    "Game timeout: " + currentGame.timeOut + "seconds" + Environment.NewLine +
                    "Game timer: " + currentGame.GameTimer.ElapsedMilliseconds / 1000 + "seconds" + Environment.NewLine +
                    "Killed ennemy: " + currentGame.Boids.Where(x => x.isDead).Count() + Environment.NewLine +
                    "Allies: " + currentGame.Boids.Where(x => x is Predator).Count() + Environment.NewLine;

            }
            else
            {
                skglControl1.Invalidate();
            }

            //fps
            {
                //counter++;
                //if (counter > 10)
                //    counter = 0;
                //else
                //{
                //    return;
                //}
                stopwatch.Stop();
                double fps = Stopwatch.Frequency / (double)stopwatch.ElapsedTicks;

                //stopwatch.Restart();
                fpsList.Enqueue(fps);
                if (fpsList.Count > fpsToRemember)
                    fpsList.Dequeue();

                FpsLabel.Text = $"{ WeighteedMovingAverage(fpsList):0.00} FPS";
            }

            //score
            {
                // ScoreLabel.Text = $"{ currentGame.score} POINT";
                ScoreLabel.Text = $"{ currentGame.Boids.Where(x => x.isDead == true).Count()} POINT";
            }
        }

        public static double WeighteedMovingAverage(Queue<double> data)
        {
            double aggregate = 0;
            double weight = 1;

            List<double> values = data.ToList();

            foreach (var d in values)
            {
                double itemWeight = values.IndexOf(d);
                aggregate += d * itemWeight;
                weight += itemWeight;
            }
            return (double)(aggregate / (double)weight);
        }

        private void skglControl1_PaintSurface(object sender, SkiaSharp.Views.Desktop.SKPaintGLSurfaceEventArgs e)
        {
            Renderer.Render(e.Surface.Canvas, currentGame,
                tails: TailsCheckbox.Checked,
                ranges: RangeCheckbox.Checked,
                direction: DirectionCheckbox.Checked);
        }

        private void AttractTrackbar_Scroll(object sender, EventArgs e)
        {
            double frac = 2.0 * AttractTrackbar.Value / AttractTrackbar.Maximum;
            currentGame.WeightFlock = Math.Pow(frac, 3);
        }

        private void AvoidTrackbar_Scroll(object sender, EventArgs e)
        {
            double frac = 2.0 * AvoidTrackbar.Value / AvoidTrackbar.Maximum;
            currentGame.WeightAvoid = Math.Pow(frac, 4);
        }

        private void AlignTrackbar_Scroll(object sender, EventArgs e)
        {
            double frac = 2.0 * AlignTrackbar.Value / AlignTrackbar.Maximum;
            currentGame.WeightAlign = Math.Pow(frac, 3);
        }

        private void VisionTrackbar_Scroll(object sender, EventArgs e)
        {
            foreach (Boid boid in currentGame.Boids)
                boid.Vision = 10 * VisionTrackbar.Value * ((boid is Predator) ? 2 : 1);
            //field.Vision = 10 * VisionTrackbar.Value;
        }

        private void skglControl1_MouseEnter(object sender, EventArgs e)
        {
            if (!currentGame.GameStatus.Equals(Game.GameStatusEnum.started))
                return;
            Cursor.Hide();
            Random rand = RandomCheckbox.Checked ? new Random() : new Random(0);
            Position position = new Position(skglControl1.PointToClient(Cursor.Position).X, skglControl1.PointToClient(Cursor.Position).Y);
            currentGame.Boids.Add(new Predator(position, rand, true));
        }

        private void skglControl1_MouseLeave(object sender, EventArgs e)
        {
            if (!currentGame.GameStatus.Equals(Game.GameStatusEnum.started))
                return;
            Cursor.Show();
            currentGame.Boids.RemoveAt(currentGame.Boids.Count - 1);
        }

        private void skglControl1_MouseMove(object sender, MouseEventArgs e)
        {

        }

        private void skglControl1_MouseHover(object sender, EventArgs e)
        {

        }
    }
}
