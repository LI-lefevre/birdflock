using Model.Environnement;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices;
using System.Text;

namespace Model
{
    public abstract class Boid
    {
        #region properties

        public abstract double hitbox { get; }
        public abstract int positionsToRemember { get; }


        public List<Position> positions = new List<Position>();
        public Position Pos;
        public Velocity Vel;
        public double TargetSpeed = 1.0;
        public bool isDead = false;
        public double Vision = 100;
        public Color color = Color.White;
        #endregion

        #region constructors

        public Boid(Boid boid)
        {
            color = boid.color;
            Pos = boid.Pos;
            Vel = boid.Vel;
            TargetSpeed = boid.TargetSpeed;
            this.positions = boid.positions;
        }

        public Boid(Position position, Random rand)
        {
            double xVel = (rand.NextDouble() - .5);
            double yVel = (rand.NextDouble() - .5);
            double targetSpeed = .5 + rand.NextDouble();

            Pos = position;
            Vel = new Velocity(xVel, yVel);
            TargetSpeed = targetSpeed;
        }

        public Boid(double Width, double Height, Random rand)
        {
            double x = rand.NextDouble() * Width;
            double y = rand.NextDouble() * Height;
            double xVel = (rand.NextDouble() - .5);
            double yVel = (rand.NextDouble() - .5);
            double targetSpeed = .5 + rand.NextDouble();

            Pos = new Position(x, y);
            Vel = new Velocity(xVel, yVel);
            TargetSpeed = targetSpeed;
        }

        #endregion

        #region Flok method
        public void FlokProcess(Game game, double Width, double Height, List<Boid> boids, double WeightFlock, double WeightAvoid, double WeightAlign)
        {
            if (isDead)
                return;
            if (this is Predator && (this as Predator).mousePredator)
            {
                return;
            }
            boids = boids.ToList().Where(x => x.Pos.Distance(Pos) < Vision).ToList().OrderBy(x => x.Pos.Distance(Pos)).ToList();
            FlockWithNeighbors(boids, .0005 * WeightFlock);
            AvoidCloseBoids(boids, .005 * WeightAvoid);
            AlignWithNeighbors(boids, .03 * WeightAlign);
            AvoidPredators(game, boids, .002);
            AvoidWalls(game.BackgroundArea, Width, Height, .2);
        }
        #region private flok methode
        private void FlockWithNeighbors(List<Boid> boids, double weight)
        {
            if (this is Predator)
                return;
            // determine mean position of the flock
            int neighborCount = 0;
            double centerX = 0;
            double centerY = 0;
            foreach (Boid boid in boids)
            {
                //if (boid.Pos.Distance(Pos) < Vision)
                {
                    centerX += boid.Pos.X;
                    centerY += boid.Pos.Y;
                    neighborCount += 1;
                }
            }
            centerX /= neighborCount;
            centerY /= neighborCount;

            // steer toward the flock
            Vel.VelX += (centerX - Pos.X) * weight;
            Vel.VelY += (centerY - Pos.Y) * weight;
        }

        private void AvoidCloseBoids(List<Boid> boids, double weight)
        {
            foreach (Boid boid in boids)
            {
                if ((boid is Predator).Equals(this is Predator)) // like avoids like
                {
                    double closeness = hitbox - boid.Pos.Distance(Pos);
                    if (closeness > 0)
                    {
                        // avoid with a magnitude correlated to closeness
                        Vel.VelX -= (boid.Pos.X - Pos.X) * weight * closeness;
                        Vel.VelY -= (boid.Pos.Y - Pos.Y) * weight * closeness;
                    }
                }
                else
                {
                    //die?
                    if (!(this is Predator) && boid.Pos.Distance(Pos) <= hitbox)
                    {
                        Predator predator = boid as Predator;
                        predator.Score += 1;
                        Die();
                    }
                }
            }
        }

        private void AlignWithNeighbors(List<Boid> boids, double weight)
        {
            // determine mean velocity of the flock
            int neighborCount = 0;
            double meanVelX = 0;
            double meanVelY = 0;
            foreach (Boid boid in boids)
            {
                if ((boid is Predator).Equals(this is Predator)) // like avoids like
                {
                    //  if (boid.Pos.Distance(Pos) < Vision)
                    {
                        meanVelX += boid.Vel.VelX;
                        meanVelY += boid.Vel.VelY;
                        neighborCount += 1;
                    }
                }
            }
            meanVelX /= neighborCount;
            meanVelY /= neighborCount;

            // steer toward the mean flock velocity
            Vel.VelX -= (Vel.VelX - meanVelX) * weight;
            Vel.VelY -= (Vel.VelY - meanVelY) * weight;
        }

        private void AvoidPredators(Game game, List<Boid> boids, double weight)
        {
            //if (this is Predator)
            //    return;
            foreach (Boid boid in boids)
            {
                if (boid.isDead)
                    continue;
                if ((boid is Predator).Equals(this is Predator))
                {
                    continue;
                }

                double i = (this is Prey) ? -1 : 0.1;
                {

                    Vel.VelX += i * (boid.Pos.X - Pos.X) * weight;
                    Vel.VelY += i * (boid.Pos.Y - Pos.Y) * weight;
                }
                if (i == 0.1)
                    continue;

            }
        }

        private void AvoidWalls(BackgroundArea backgroundArea, double width, double height, double turn)
        {
            // double distancetoREctangle = distance(backgroundArea.Borders.First(), Pos);
            List<Rectangle> AllLimits = backgroundArea.AllLimits.OrderBy(x => Pos.distanceRectangle(x, out Position _)).ToList();
            for (int i = 0; i < Math.Min(2, AllLimits.Count); i++)
            {
                Rectangle rectangle = AllLimits[i];
                double d = Pos.distanceRectangle(rectangle, out Position nearest);
                if (d < Vision)
                {
                    (double x, double y) delta = Pos.Delta(nearest);
                    if (delta.x != 0 && Math.Abs(delta.x) > Math.Abs(delta.y))
                        Vel.VelX -= turn * (10 / delta.x);
                    if (delta.y != 0 && Math.Abs(delta.y) > Math.Abs(delta.x))
                        Vel.VelY -= turn * (10 / delta.y);
                }

            }

            //if (Pos.X < Vision) Vel.VelX += turn * (10 / Pos.X);
            //if (Pos.Y < Vision) Vel.VelY += turn * (10 / Pos.Y);
            //if (Pos.X > width - Vision) Vel.VelX -= turn * (10 / (width - Pos.X));
            //if (Pos.Y > height - Vision) Vel.VelY -= turn * (10 / (height - Pos.Y));
        }


        #endregion
        #endregion

        #region individual method

        public void Advance(double elapsedTime, BackgroundArea BackgroundArea)
        {

            if (isDead)
                return;
            Vel.SetSpeed(TargetSpeed, elapsedTime);
            Pos.Move(Vel, BackgroundArea, elapsedTime);

            positions.Add(Pos);
            while (positions.Count > positionsToRemember)
                positions.RemoveAt(0);
        }

        public void Die()
        {
            positions.RemoveRange(0, positions.Count);
            Vel.VelX = 0;
            Vel.VelY = 0;
            isDead = true;
        }
        #endregion
    }
}
