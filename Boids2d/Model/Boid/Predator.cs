﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Model
{
    public class Predator : Boid, INotifyPropertyChanged
    {
        #region properties
        public override double hitbox => 20 * 1.5f;
        public override int positionsToRemember => 20;
        public bool mousePredator;
        private int score = 0;
        public int Score
        {
            get { return score; }
            set
            {
                if (value != score)
                {
                    score = value;
                    OnPropertyChanged(null);
                }
            }
        }
        private Color originColor = Color.Yellow;
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion
        #region event
        protected void OnPropertyChanged(PropertyChangedEventArgs e)
        {
            this.color = Color.FromArgb(Convert.ToInt32(originColor.A), Convert.ToInt32(originColor.R *Math.Max(0,(100-score)) / 100), Convert.ToInt32(originColor.G * 100 / 100), Convert.ToInt32(originColor.B * Math.Max(0, (100 - score)) / 100));
            ////this.color = ControlPaint.Dark(color, 0.0001f);

        }
        #endregion
        #region constructor

        public Predator(Boid boid) : base(boid)
        {
            // predators have double distance
            if (boid is Prey)
            {
                Vision *= 2;
                TargetSpeed += .5;
            }
                color = originColor;
            if (boid is Predator)
                Score = (boid as Predator).Score;
        }

        public Predator(double Width, double Height, Random rand, bool mousePredator = false) : base(Width, Height, rand)
        {
            this.mousePredator = mousePredator;
            // predators have double distance
            Vision *= 2;
            TargetSpeed += .5;
            if (mousePredator)
                TargetSpeed = 0;
            color = originColor;
        }
        public Predator(Position position, Random rand, bool mousePredator = false) : base(position, rand)
        {
            this.mousePredator = mousePredator;
            // predators have double distance
            Vision *= 2;
            TargetSpeed += .5;
            if (mousePredator)
                TargetSpeed = 0;
            color = originColor;
        }
        #endregion
        #region method

        #endregion
    }
}
