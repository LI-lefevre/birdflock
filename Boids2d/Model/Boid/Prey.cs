﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace Model
{
    public class Prey : Boid
    {
        public override double hitbox => 20;
        public override int positionsToRemember => 10;
        public Prey(Boid boid) : base(boid)
        {
            if (boid is Predator)
            {
                Vision /= 2;
                TargetSpeed -= .5;
            }
            this.isDead = boid.isDead;
            color = Color.White;
        }

        public Prey(double Width, double Height, Random rand) : base(Width, Height, rand)
        {
        }


    }
}
