﻿using Model.Environnement;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Model
{
    public class Game
    {
        #region properties

        public BackgroundArea BackgroundArea;
        public double Width;
        public double Height;
        public readonly List<Boid> Boids;
        public bool graphicalEvolution;
        public int timeOut;
        public double WeightFlock = 1.0;
        public double WeightAvoid = 1.0;
        public double WeightAlign = 1.0;
        public Stopwatch GameTimer = new Stopwatch();

        public GameStatusEnum GameStatus = GameStatusEnum.initialized;
        public enum GameStatusEnum { initialized, started, victory, lost, paused }
        #endregion

        #region constructor
        public Game(BackgroundArea backgroundArea, double width, double height, int boidCount, int predatorCount, bool random, bool graphicalEvolution = true, int timeOut = 60)
        {
            BackgroundArea = backgroundArea;
            Width = width;
            Height = height;
            Boids = new List<Boid>(boidCount);
            RandomizeBoids(predatorCount, random);
            this.graphicalEvolution = graphicalEvolution;
            this.timeOut = timeOut;
            GameStatus = GameStatusEnum.initialized;
        }

        #endregion

        #region method
        #region status method

        public void start()
        {
            GameTimer.Start();
            GameStatus = GameStatusEnum.started;
        }
        public void pause()
        {
            GameTimer.Stop();
            GameStatus = GameStatusEnum.paused;
        }
        public void finish(bool win = true)
        {
            GameTimer.Stop();
            if (win)
                GameStatus = GameStatusEnum.victory;
            else
                GameStatus = GameStatusEnum.lost;
        }
        #endregion
        private void RandomizeBoids(int predatorCount, bool random)
        {
            Random rand = random ? new Random() : new Random(0);
            for (int i = 1; i <= Boids.Capacity; i++)
            {
                bool first = true;
                Boid boid = null;
                Rectangle rectangle = new Rectangle();
                while (first || rectangle.Contains(boid.Pos.Point))
                {
                    first = false;
                    boid = new Prey(Width, Height, rand);

                    if (i < predatorCount)
                        boid = new Predator(Width, Height, rand);

                    rectangle = BackgroundArea.AllLimits.OrderBy(x => boid.Pos.distanceRectangle(x, out Position _)).First();
                }
                Boids.Add(boid);
            }
        }


        public void Advance(Point point, double elapsedTime = 0.10)
        {
            if (GameStatus != GameStatusEnum.started)
                return;

            //manage mouse
            {
                if (point.X > 0 && point.Y > 0 && point.X < Width && point.Y < Height && Boids.LastOrDefault() != null && Boids.Last() is Predator)
                {
                    Predator mousePredator = Boids.Last() as Predator;
                    double weight = 0.01;
                    mousePredator.Vel.VelX += (point.X - mousePredator.Pos.X) * weight;
                    mousePredator.Vel.VelY += (point.Y - mousePredator.Pos.Y) * weight;
                }
            }

            ParallelOptions parallelOptions = new ParallelOptions();
            parallelOptions.MaxDegreeOfParallelism =  Environment.ProcessorCount;
            Parallel.ForEach(Boids.Where(x=>!x.isDead), parallelOptions, boid =>
            {
                boid.FlokProcess(this, Width, Height, Boids, WeightFlock, WeightAvoid, WeightAlign);
                boid.Advance(elapsedTime, BackgroundArea);
            }
            );
            if (Boids.Where(x => x is Prey).Count() <= Boids.Where(y => y.isDead).Count())
                finish();
            //if (GameTimer.ElapsedMilliseconds / 1000 > timeOut)
            //    finish(false);
        }

        #endregion
    }
}
