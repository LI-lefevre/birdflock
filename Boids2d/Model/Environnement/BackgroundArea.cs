﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace Model.Environnement
{
    public class BackgroundArea
    {
        public List<Rectangle> AllLimits
        {
            get
            {
                List<Rectangle> value = new List<Rectangle>();
                value.AddRange(Borders);
                value.AddRange(Walls);
                return value;
            }
        }
        public Rectangle playableArea;
        public List<Rectangle> Borders { get; set; }
        private List<Rectangle> Walls { get; set; }
        public BackgroundArea(int Width, int Height, List<Rectangle> walls = null)
        {
            playableArea = new Rectangle(0, 0, Width, Height);
            Borders = new List<Rectangle>() { new Rectangle(0, 0, Width, 5), new Rectangle(0, 0, 5, Height), new Rectangle(0, Height - 5, Width, 5), new Rectangle(Width - 5, 0, 5, Height) };
            if (walls == null)
                Walls = new List<Rectangle>();
            else
                Walls = walls;
        }
    }
}
