﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model
{
    public struct Velocity
    {
        public double VelX;
        public double VelY;

        private static double maxSpeed = 3;//This is the maximum speed that the object will achieve
        private static double maxAcceleration = 1;//How fast will object reach a maximum speed

        private static double epsilon = 0.01;

        public Velocity(double x, double y)
        {
            VelX = x;
            VelY = y;
        }

        public double GetAngle()
        {
            if (VelX == 0 && VelY == 0)
                return 0;
            double angle = Math.Atan(VelY / VelX) * 180 / Math.PI - 90;
            if (VelX < 0)
                angle += 180;
            return angle;
        }

        public void SetSpeed(double targetSpeed, double elapsedTime)
        {
            if (VelX == 0 && VelY == 0)
                return;
            //avoid infinite velocity trouble
            {
                if (Double.IsNaN(VelX))
                    VelX = maxSpeed;
                if (Double.IsNaN(VelY))
                    VelY = maxSpeed;
                VelX = Math.Sign(VelX) * Math.Min(maxSpeed, Math.Abs(VelX));
                VelY = Math.Sign(VelY) * Math.Min(maxSpeed, Math.Abs(VelY));
            }
            var currentSpeed = Math.Sqrt(VelX * VelX + VelY * VelY);

            double targetVelX = (VelX / currentSpeed) * targetSpeed;
            double targetVelY = (VelY / currentSpeed) * targetSpeed;

         
            double AccX = targetVelX - VelX;
            double AccY = targetVelY - VelY;      

            //don't reach max ecceleration
            VelX += Math.Min(Math.Abs(AccX), maxAcceleration) * Math.Sign(AccX) * 0.5;
            VelY += Math.Min(Math.Abs(AccY), maxAcceleration) * Math.Sign(AccY) * 0.5;


            //don't reach the max speed
            VelX = Math.Min(Math.Abs(VelX), maxSpeed) * Math.Sign(VelX);
            VelY = Math.Min(Math.Abs(VelY), maxSpeed) * Math.Sign(VelY);

 
        }
    }
}