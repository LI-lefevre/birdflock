﻿using Model.Environnement;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace Model
{
    public struct Position
    {
        public double X;
        public double Y;
        public Point Point
        {
            get
            {
                return new Point(Convert.ToInt32(X), Convert.ToInt32(Y));
            }
        }
        public Position(double x, double y)
        {
            X = x;
            Y = y;
        }

        public void Shift(Position pos)
        {
            X += pos.X;
            Y += pos.Y;
        }

        public void Move(Velocity vel, BackgroundArea BackgroundArea, double elapsedTime)
        {
            double _x = X;
            double _y = Y;
            X += vel.VelX * elapsedTime;
            Y += vel.VelY * elapsedTime;

            //don't reach the limit of the screen
            {
                Position previousPosition = new Position(_x, _y);

                Rectangle rectangle = BackgroundArea.AllLimits.OrderBy(x => previousPosition.distanceRectangle(x, out Position _)).First();
                if (rectangle.Contains(this.Point) || LineIntersectsRect(previousPosition, this, rectangle))
                {
                    X = previousPosition.X;
                    Y = previousPosition.Y;
                }
            }
        }

        public (double x, double y) Delta(Position otherPosition)
        {
            return (otherPosition.X - X, otherPosition.Y - Y);
        }

        public double Distance(Position otherPosition)
        {
            (double dX, double dY) = Delta(otherPosition);
            return Math.Sqrt(dX * dX + dY * dY);
        }
        public double distanceRectangle(Rectangle rect, out Position nearest)
        {
            nearest = this;
            if (X < rect.Left)
                nearest.X = rect.Left;
            else if (X > rect.Right)
                nearest.X = rect.Right;

            if (Y < rect.Top)
                nearest.Y = rect.Top;
            else if (Y > rect.Bottom)
                nearest.Y = rect.Bottom;
            return nearest.Distance(this);
        }

        public static bool LineIntersectsRect(Position p1, Position p2, Rectangle r)
        {
            return LineIntersectsLine(p1, p2, new Position(r.X, r.Y), new Position(r.X + r.Width, r.Y)) ||
                   LineIntersectsLine(p1, p2, new Position(r.X + r.Width, r.Y), new Position(r.X + r.Width, r.Y + r.Height)) ||
                   LineIntersectsLine(p1, p2, new Position(r.X + r.Width, r.Y + r.Height), new Position(r.X, r.Y + r.Height)) ||
                   LineIntersectsLine(p1, p2, new Position(r.X, r.Y + r.Height), new Position(r.X, r.Y)) ||
                   (r.Contains(p1.Point) && r.Contains(p2.Point));
        }

        private static bool LineIntersectsLine(Position l1p1, Position l1p2, Position l2p1, Position l2p2)
        {
            double q = (l1p1.Y - l2p1.Y) * (l2p2.X - l2p1.X) - (l1p1.X - l2p1.X) * (l2p2.Y - l2p1.Y);
            double d = (l1p2.X - l1p1.X) * (l2p2.Y - l2p1.Y) - (l1p2.Y - l1p1.Y) * (l2p2.X - l2p1.X);

            if (d == 0)
            {
                return false;
            }

            double r = q / d;

            q = (l1p1.Y - l2p1.Y) * (l1p2.X - l1p1.X) - (l1p1.X - l2p1.X) * (l1p2.Y - l1p1.Y);
            double s = q / d;

            if (r < 0 || r > 1 || s < 0 || s > 1)
            {
                return false;
            }

            return true;
        }
    }
}
